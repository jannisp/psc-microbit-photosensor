# Microbit Origami Plant
## Idea
TODO

## Set-up
### Prerequisites
- A micro:bit connected to the computer via USB
- Browser with internet access

### Flash the program
A program has to be in a special compiled format (`.hex`). The easiest way to do that, is via the [online editor](https://makecode.microbit.org/#editor), where you can also switch to Scratch, if you prefer using blocks for programming.

Copy paste the `photosensor.py` and click **download** on the bottom left.

Then you can just copy the downloaded `.hex` file to the microbit storage and it will take care of the rest. The micro:bit should start running the program after a couple of seconds.

### Compile `.hex` locally
Alternatively you can try to compile the needed file directly on your computer. However, this is rather advanced and you need access to an `arm64` architecture. Detailed instructions can be found [here](https://microbit-micropython.readthedocs.io/en/v2-docs/devguide/hexformat.html).