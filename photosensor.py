def on_forever():
    # 50 is an arbitrary value and can be adapted to match the light conditions
    if pins.analog_read_pin(AnalogPin.P1) > 50:
        basic.show_icon(IconNames.HAPPY)
        # The servo angle has to be adapted to the origami model
        servos.P0.set_angle(30)
    else:
        basic.show_icon(IconNames.SAD)
        servos.P0.set_angle(0)

basic.forever(on_forever)